# artifacts

Third-party files downloaded here, while binary, are intended to be included in something else like a Docker image so might not be appropriate to run with `plz run` as they might not match the host architecture.
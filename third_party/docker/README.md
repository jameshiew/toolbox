# docker

Third-party Docker images should be referenced from here, then they can be used as base images elsewhere in this repo.

Efforts made to pin to immutable tags of third-party Docker images.